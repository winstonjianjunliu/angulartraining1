import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms'

@Component({
  selector: 'app-renderer',
  templateUrl: './renderer.component.html',
  styleUrls: ['./renderer.component.css']
})
export class RendererComponent implements OnInit {

  constructor() { }

  

  urlOptions = [
    { valueUrl: 'https://via.placeholder.com', optionText: 'placeholder' },
    { valueUrl: 'https://picsum.photos', optionText: 'general' },
    { valueUrl: 'https://placekitten.com', optionText: 'kitten' },
    { valueUrl: 'https://placebear.com', optionText: 'bear' },
    { valueUrl: 'https://placebeard.it', optionText: 'beard' },
    { valueUrl: 'http://www.fillmurray.com', optionText: 'Phil' },
    { valueUrl: 'https://www.placecage.com', optionText: 'Nicholas' },
    { valueUrl: 'https://stevensegallery.com', optionText: 'Steven' }
  ]

  selectedText = ''
  selectedUrl = ''

  selectedWidth
  selectedHeight
  
  ngOnInit() {
    this.selectedText = this.urlOptions[0].optionText
    this.selectedWidth = 200
    this.selectedHeight = 200
    this.selectedUrl = this.urlOptions[0].valueUrl + "/" +  this.selectedWidth + "/" + this.selectedHeight
    
    console.log(this.selectedUrl)
  }

  //function with no usage
  public onChange(event): void {  // event will give you full breif of action
    const newVal = event.target.value;
    console.log(newVal)
    this.selectedUrl = newVal + "/200/300"
  }

  
  public onSubmit(form: NgForm) {
    this.selectedWidth = form.value.width
    this.selectedHeight = form.value.height
    this.selectedUrl = form.value.selectedUrl +"/"+this.selectedWidth + "/" + this.selectedHeight
    
    console.log(form.value)
  }

}
