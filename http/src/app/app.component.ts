import { Component, OnInit } from '@angular/core';
import { User } from './models/user'
import { GetUsersService } from './get-users.service'
import {NgForm} from '@angular/forms'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  title = 'http';

  usersModel //= 

  userUrl = 'https://jsonplaceholder.typicode.com/users'

  user = "1"

  userModel = new User()

  disabled = true

  constructor (private userService:GetUsersService) {

  }

  ngOnInit(): void {
    this.userService.getUsers().subscribe((results) => {
      this.usersModel = results.slice(0, 8)
    })

    this.userService.getUser( this.userUrl + "/" + this.user).subscribe((result) => {
      this.userModel = result 
    })
  }

    
  onSubmit(form: NgForm) {
    this.user = form.value.selectedUser

    this.userService.getUser( this.userUrl + "/" + this.user).subscribe((result) => {
      this.userModel = result 
    })
    
    console.log(this.userUrl + "/" + this.user)
  }

  onEdit(event) {
    this.disabled = false;
  }

  onChange(form: NgForm) {
    this.disabled = true;

    this.userModel.name = form.value.name
    this.userModel.email = form.value.email
    this.userModel.address.city = form.value.city
    this.userModel.company.name = form.value.company
    
    console.log(form.value)
  }


} 
