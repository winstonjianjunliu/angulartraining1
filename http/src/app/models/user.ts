import { Address } from './address';
import { Company } from './company';

// declare a class 
export class User {
    // class properties
    id:number = 1
    name:string = "Winston Liu"
    username:string
    email:string = "winstonjianjun@gmail.com"
    address: Address = new Address()
    phone:string
    website:string
    company: Company = new Company()

    // we can declare an optional constructor
    constructor() {
        
    }

}