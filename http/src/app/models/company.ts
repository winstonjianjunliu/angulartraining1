// declare a class 
export class Company {
    // class properties
    name:string
    catchPhrase: string
    bs: string

    // we can declare an optional constructor
    constructor() {
        
    }

}