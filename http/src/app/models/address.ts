// declare a class 
export class Address {
    // class properties
    street: string
    suite: string
    city: string
    zipcode: string
    geo

    // we can declare an optional constructor
    constructor() {
        
    }

}