import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { User } from './models/user';
import { Observable, throwError } from 'rxjs';
import { catchError, retry} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GetUsersService {

  // all users
  usersUrl = 'https://jsonplaceholder.typicode.com/users'

  constructor(private http: HttpClient) { }

  //version1
  // getSymbols() {
  //   return this.http.get(this.stockUrl)
  // }

  //version2
  // getSymbols(): Observable<Stock[]> {
  //   return this.http.get<Stock[]>(this.stockUrl)
  // }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl)
      // handle any errors
      .pipe(retry(3), // retry a failed request up to 3 times
        catchError(this.handleError) // then handle the error
      );
  }

  getUser(userUrl): Observable<User> {
    return this.http.get<User>(userUrl)
      // handle any errors
      .pipe(retry(3), // retry a failed request up to 3 times
        catchError(this.handleError) // then handle the error
      );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message)
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`)
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.')
  }


}