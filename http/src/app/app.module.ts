import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
 
import { AppComponent } from './app.component';

import { RendererComponent } from './renderer/renderer.component';
import { GetUsersService } from './get-users.service';

@NgModule({
  declarations: [
    AppComponent,
    RendererComponent
  ],
  imports: [
    BrowserModule, FormsModule, HttpClientModule
  ],
  providers: [GetUsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
